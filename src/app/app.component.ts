import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table'; 
import {MatSnackBar} from '@angular/material/snack-bar';

import { Subject } from 'rxjs';

import { CuboNetworkModel } from './services/cubonetworkmodel';
import { CuboNetworkService } from './services/cubonetwork.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [CuboNetworkService],
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  durationInSeconds = 5;

  save: FormGroup;
  firstName = new FormControl('');
  lastName  = new FormControl('');
  participation  = new FormControl('');

  view: any[] = [400, 400];
  models: any[] = [];
  modelTable: CuboNetworkModel[];

  displayedColumns: string[] = ['cuboId', 'firstName', 'lastName', 'participation'];
  dataSource = new MatTableDataSource<CuboNetworkModel>(this.modelTable);

  constructor(fb: FormBuilder, 
              private cubonetworkservice: CuboNetworkService,
              private snackBar: MatSnackBar) {
    this.save = fb.group({
      firstName: this.firstName,
      lastName: this.lastName,
      participation: this.participation
    });
  }

  ngOnInit() {
    this.getPeoples();
  }

  getPeoples(): void {
    this.models = [];
    this.cubonetworkservice.getPeoples()
      .subscribe(modelTable => {
        this.modelTable = modelTable; 
        modelTable.map(item =>{
          console.log(item);
          this.models.push({
            name: item.firstName + ' ' + item.lastName,
            value: item.participation
          })
        });
        this.models = [...this.models];
      });
  }

  getErrorMessage() {
    if (this.firstName.hasError('required')) {
      return 'You must enter a value';
    }
    if (this.lastName.hasError('required')) {
      return 'You must enter a value';
    }
    if (this.participation.hasError('required')) {
      return 'You must enter a value';
    }
    if (this.participation.hasError('required')) {
      return 'You must enter a value';
    }

    return '';
  }

  projectReset() {
    this.cubonetworkservice.resetPeople()
      .subscribe(saved => {
        this.snackBar.open("Project Reset", 'Ok', {
          duration: 5000,
        });
        this.getPeoples();
      }, error =>{
        this.snackBar.open(error.error.message, 'Ok', {
          duration: 5000,
        });
      });
  }

  submitSave(event) {
    console.log("First Name"   , this.firstName.value);
    console.log("Last Name"    , this.lastName.value);
    console.log("Participation", this.participation.value);

    const editModel:CuboNetworkModel = new CuboNetworkModel("-1", this.firstName.value, this.lastName.value, this.participation.value);

    this.cubonetworkservice
      .addPeople(editModel)
      .subscribe(saved => {
        this.getPeoples();
        this.save.reset();
      }, error =>{
        this.snackBar.open(error.error.message, 'Ok', {
          duration: 5000,
        });
      });
  }
}
