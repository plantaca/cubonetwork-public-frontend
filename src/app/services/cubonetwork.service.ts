import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CuboNetworkModel } from './cubonetworkmodel';

@Injectable({ providedIn: 'root' })
export class CuboNetworkService {

  private apiUrl = 'https://zfqa2bkgy6.execute-api.us-east-1.amazonaws.com/dev/people-participation';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }).append("x-api-key", "mVSOxdrGWx3TOIgMzuRhI8pf3UYeWIDC3aIXS0SX")
  };

  constructor(
    private http: HttpClient) { }

  /**
   * Read all data
   */
  getPeoples (): Observable<CuboNetworkModel[]> {
    return this.http.get<CuboNetworkModel[]>(this.apiUrl, this.httpOptions)
      .pipe(
        catchError(this.handleError<CuboNetworkModel[]>('getPeoples', []))
      );
  }

  /**
   * Save new register.
   * @param CuboNetworkModel
   */
  addPeople (model: CuboNetworkModel): Observable<CuboNetworkModel> {
    return this.http.post<CuboNetworkModel>(this.apiUrl, model, this.httpOptions);
  }

  /**
   * Delete all data
   * @param CuboNetworkModel
   */
  resetPeople (): Observable<CuboNetworkModel> {
    return this.http.delete<CuboNetworkModel>(this.apiUrl, this.httpOptions);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log("show error");
      console.error(error);
      return of(result as T);
    };
  }
}