export class CuboNetworkModel {

    constructor(
      public cuboId: string,
      public firstName: string,
      public lastName: string,
      public participation: number
    ) {  }
  
  }